#include <stdlib.h>
#include <stdio.h>
#include "duelo.h"


void alloCards (struct Data* cardsData)
{
    cardsData->cards = malloc (cardsData->data[0] * sizeof (struct Cards));
}

void deallocCards(struct Data* cardsData){

    free(cardsData->cards);
}

void readData (struct Data* cardsData)
{
    do
    {
        scanf ("%d", &cardsData->data[0]);
        scanf ("%d", &cardsData->data[1]);
        scanf ("%d", &cardsData->data[2]);
        scanf ("%d", &cardsData->data[3]);

    } while (cardsData->data[0] < 1 || cardsData->data[1] < 1 || cardsData->data[2] < 1 || cardsData->data[3] < 1 ); // o do while não aceitou que eu agrupasse as verificações, infelizmente
    
        
}

void readCards (struct Data *cardsData)
{
    int verify = 0;
    for (int i = 0; i < cardsData->data[0]; i++)
    {
        verify = 0;
        scanf("%d", &cardsData->cards[i].attribute[0]);
        scanf("%d", &cardsData->cards[i].attribute[1]);
        scanf("%d", &cardsData->cards[i].attribute[2]);   

        /*não deu pra colocar as verificações de entrada no mesmo while e nem
        para agrupar em parêntesis as variáveis a serem analisadas, pois por alguma razão
        quando eu fazia isso ele não verificava corretamente. desta forma, estou fazendo a verificação
        de uma forma mais extensa. 
        */
      
        if (cardsData->cards[i].attribute[0] < 1 || cardsData->cards[i].attribute[1] < 1 || 
        cardsData->cards[i].attribute[2] < 1 || cardsData->cards[i].attribute[0] > 50 ||
        cardsData->cards[i].attribute[1] > 50 || cardsData->cards[i].attribute[2] > 50 )
            verify = 1;
        else
            verify = 0;
        
        while (verify)
        {
            if (cardsData->cards[i].attribute[0] < 1 || cardsData->cards[i].attribute[1] < 1 || 
            cardsData->cards[i].attribute[2] < 1 || cardsData->cards[i].attribute[0] > 50 ||
            cardsData->cards[i].attribute[1] > 50 || cardsData->cards[i].attribute[2] > 50 )
            {
                scanf("%d", &cardsData->cards[i].attribute[0]);
                scanf("%d", &cardsData->cards[i].attribute[1]);
                scanf("%d", &cardsData->cards[i].attribute[2]);  
            }else
                verify = 0;  
        }                   

    }
    
}



int judge(struct Data cardsData, int addingUp0, int addingUp1, int addingUp2, int count)
{
    
    if (addingUp0 == cardsData.data[1] && addingUp1 == cardsData.data[2] && addingUp2 == cardsData.data[3])
        return 1;
    else if (cardsData.data[0] -1 == count)
        return 0;
 
    if (cardsData.cards[count+1].attribute[0] >= cardsData.data[1] || 
    cardsData.cards[count+1].attribute[1] >= cardsData.data[2] || 
    cardsData.cards[count+1].attribute[2] >= cardsData.data[3])
    {
        return judge(cardsData, addingUp0, addingUp1, addingUp2, count+1);
    }
        
 
    int ignoringIt = judge(cardsData, addingUp0, addingUp1, addingUp2, count+1);

    int consideringIt = judge(cardsData, addingUp0 + cardsData.cards[count+1].attribute[0],
    addingUp1 +cardsData.cards[count+1].attribute[1], addingUp2 + cardsData.cards[count+1].attribute[2],  count+1);

    return ignoringIt || consideringIt;
           
}

void Judge (struct Data cardsData)
{
    int addingUp0 = 0, addingUp1 = 0, addingUp2 = 0;
    int count = 0;

    int Return = judge(cardsData, addingUp0, addingUp1, addingUp2, count);

    if (Return)
        printf("Y\n");
    else
        printf ("N\n");
}


