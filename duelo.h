# ifndef duelo_h

#define duelo_h

struct Cards
{
    int attribute[3];
};

struct Data
{
    int data[4];
    struct Cards* cards;
    
};

void readData (struct Data* cardsData);

void alloCards (struct Data* cardsData);

void readCards (struct Data *cardsData);

int judge (struct Data cardsData, int addings0, int addings1, int addings2, int count);

void Judge (struct Data cardsData);



void deallocCards(struct Data* cardsData);




# endif
