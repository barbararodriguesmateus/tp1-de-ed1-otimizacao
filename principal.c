#include <stdio.h>
#include "duelo.h"

int main ()
{
    struct Data DataMain;

    //printf ("Digite as informacoes: \n");
    readData(&DataMain);


    alloCards(&DataMain);

    //printf ("Digite os atributos de cada carta: \n");
    readCards(&DataMain);

    Judge(DataMain);

    deallocCards(&DataMain);


    return 0; 
}